# Fetching and building against HepMC3 with modern CMake

This should work:

```
$ # configure
$ mkdir build && cmake -S . -B build
$ # build
$ cmake --build build
```

<details>
<summary>Click here for build logs</summary>

```
~/S/w/hepmc_and_cmake cmake -S . -B build
CMake Deprecation Warning at build/_deps/hepmc-src/CMakeLists.txt:1 (cmake_minimum_required):
  Compatibility with CMake < 3.5 will be removed from a future version of
  CMake.

  Update the VERSION argument <min> value or use a ...<max> suffix to tell
  CMake that the project does not need compatibility with older versions.


-- HepMC3 Build type not specified, use: Release
-- HepMC3: CMAKE_VERSION=3.27.5
-- HepMC3: HEPMC3_ENABLE_SEARCH         OFF
-- HepMC3: HEPMC3_ENABLE_ROOTIO         OFF
-- HepMC3: HEPMC3_ENABLE_PROTOBUFIO     OFF
-- HepMC3: HEPMC3_ENABLE_PYTHON         OFF
-- HepMC3: HEPMC3_PYTHON_VERSIONS       2.X,3.X
-- HepMC3: HEPMC3_ENABLE_TEST           OFF
-- HepMC3: HEPMC3_BUILD_DOCS            OFF
-- HepMC3: HEPMC3_BUILD_EXAMPLES        OFF
-- HepMC3: HEPMC3_INSTALL_INTERFACES    OFF
-- HepMC3: HEPMC3_ROOTIO_INSTALL_LIBDIR lib
-- HepMC3: HEPMC3_CXX_STANDARD 11
-- HepMC3: CMAKE_CXX_COMPILER_ID GNU
-- HepMC3: CMAKE_CXX_FLAGS  -Wno-strict-aliasing -O3 -DNDEBUG
-- HepMC3: CMAKE_C_COMPILER_ID GNU
-- HepMC3::All contains: HepMC3::HepMC3
-- HepMC3::All_static contains: HepMC3::HepMC3_static
-- HepMC3: Developers only: black found in /usr/bin/black. Formating of Python code is possible.
-- HepMC3: Developers only: cppcheck found in /usr/bin/cppcheck. Static analysis of C++ code is possible.
-- Configuring done (0.2s)
-- Generating done (0.0s)
-- Build files have been written to: /home/blue/Scratchpad/work/hepmc_and_cmake/build
~/S/w/hepmc_and_cmake
~/S/w/hepmc_and_cmake cmake --build build
[  2%] Building CXX object _deps/hepmc-build/CMakeFiles/HepMC3.dir/src/GenCrossSection.cc.o
[  4%] Building CXX object _deps/hepmc-build/CMakeFiles/HepMC3.dir/src/GenParticle.cc.o
[  6%] Building CXX object _deps/hepmc-build/CMakeFiles/HepMC3.dir/src/GenVertex.cc.o
[  9%] Building CXX object _deps/hepmc-build/CMakeFiles/HepMC3.dir/src/Print.cc.o
[ 11%] Building CXX object _deps/hepmc-build/CMakeFiles/HepMC3.dir/src/Setup.cc.o
[ 13%] Building CXX object _deps/hepmc-build/CMakeFiles/HepMC3.dir/src/WriterHEPEVT.cc.o
[ 15%] Building CXX object _deps/hepmc-build/CMakeFiles/HepMC3.dir/src/GenEvent.cc.o
[ 18%] Building CXX object _deps/hepmc-build/CMakeFiles/HepMC3.dir/src/GenPdfInfo.cc.o
[ 20%] Building CXX object _deps/hepmc-build/CMakeFiles/HepMC3.dir/src/HEPEVT_Wrapper.cc.o
[ 22%] Building CXX object _deps/hepmc-build/CMakeFiles/HepMC3.dir/src/ReaderAscii.cc.o
[ 25%] Building CXX object _deps/hepmc-build/CMakeFiles/HepMC3.dir/src/ReaderHEPEVT.cc.o
[ 27%] Building CXX object _deps/hepmc-build/CMakeFiles/HepMC3.dir/src/WriterAscii.cc.o
[ 29%] Building CXX object _deps/hepmc-build/CMakeFiles/HepMC3.dir/src/GenHeavyIon.cc.o
[ 31%] Building CXX object _deps/hepmc-build/CMakeFiles/HepMC3.dir/src/GenRunInfo.cc.o
[ 34%] Building CXX object _deps/hepmc-build/CMakeFiles/HepMC3.dir/src/LHEFAttributes.cc.o
[ 36%] Building CXX object _deps/hepmc-build/CMakeFiles/HepMC3.dir/src/ReaderAsciiHepMC2.cc.o
[ 38%] Building CXX object _deps/hepmc-build/CMakeFiles/HepMC3.dir/src/ReaderLHEF.cc.o
[ 40%] Building CXX object _deps/hepmc-build/CMakeFiles/HepMC3.dir/src/WriterAsciiHepMC2.cc.o
[ 43%] Building CXX object _deps/hepmc-build/CMakeFiles/HepMC3.dir/src/ReaderPlugin.cc.o
[ 45%] Building CXX object _deps/hepmc-build/CMakeFiles/HepMC3.dir/src/WriterPlugin.cc.o
[ 47%] Linking CXX shared library outputs/lib/libHepMC3.so
[ 47%] Built target HepMC3
[ 50%] Building CXX object CMakeFiles/main.dir/main.cpp.o
[ 52%] Linking CXX executable main
[ 52%] Built target main
[ 54%] Building CXX object _deps/hepmc-build/CMakeFiles/HepMC3_static.dir/src/GenCrossSection.cc.o
[ 56%] Building CXX object _deps/hepmc-build/CMakeFiles/HepMC3_static.dir/src/GenParticle.cc.o
[ 59%] Building CXX object _deps/hepmc-build/CMakeFiles/HepMC3_static.dir/src/GenVertex.cc.o
[ 61%] Building CXX object _deps/hepmc-build/CMakeFiles/HepMC3_static.dir/src/Print.cc.o
[ 63%] Building CXX object _deps/hepmc-build/CMakeFiles/HepMC3_static.dir/src/Setup.cc.o
[ 65%] Building CXX object _deps/hepmc-build/CMakeFiles/HepMC3_static.dir/src/WriterHEPEVT.cc.o
[ 68%] Building CXX object _deps/hepmc-build/CMakeFiles/HepMC3_static.dir/src/GenEvent.cc.o
[ 70%] Building CXX object _deps/hepmc-build/CMakeFiles/HepMC3_static.dir/src/GenPdfInfo.cc.o
[ 72%] Building CXX object _deps/hepmc-build/CMakeFiles/HepMC3_static.dir/src/HEPEVT_Wrapper.cc.o
[ 75%] Building CXX object _deps/hepmc-build/CMakeFiles/HepMC3_static.dir/src/ReaderAscii.cc.o
[ 77%] Building CXX object _deps/hepmc-build/CMakeFiles/HepMC3_static.dir/src/ReaderHEPEVT.cc.o
[ 79%] Building CXX object _deps/hepmc-build/CMakeFiles/HepMC3_static.dir/src/WriterAscii.cc.o
[ 81%] Building CXX object _deps/hepmc-build/CMakeFiles/HepMC3_static.dir/src/GenHeavyIon.cc.o
[ 84%] Building CXX object _deps/hepmc-build/CMakeFiles/HepMC3_static.dir/src/GenRunInfo.cc.o
[ 86%] Building CXX object _deps/hepmc-build/CMakeFiles/HepMC3_static.dir/src/LHEFAttributes.cc.o
[ 88%] Building CXX object _deps/hepmc-build/CMakeFiles/HepMC3_static.dir/src/ReaderAsciiHepMC2.cc.o
[ 90%] Building CXX object _deps/hepmc-build/CMakeFiles/HepMC3_static.dir/src/ReaderLHEF.cc.o
[ 93%] Building CXX object _deps/hepmc-build/CMakeFiles/HepMC3_static.dir/src/WriterAsciiHepMC2.cc.o
[ 95%] Building CXX object _deps/hepmc-build/CMakeFiles/HepMC3_static.dir/src/ReaderPlugin.cc.o
[ 97%] Building CXX object _deps/hepmc-build/CMakeFiles/HepMC3_static.dir/src/WriterPlugin.cc.o
[100%] Linking CXX static library outputs/lib/libHepMC3-static.a
[100%] Built target HepMC3_static
```

</details>
